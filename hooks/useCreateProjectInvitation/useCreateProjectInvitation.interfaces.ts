/* eslint-disable @typescript-eslint/indent */
import { ApolloError } from '@apollo/react-hooks';
import {
  IApolloLinkOptions,
  IApolloMutation,
  IApolloResponseVariables,
  IOnCompleted,
  IOnError,
  PartialPick,
} from '@netfront/common-library';
import { IDBInvitation } from '@netfront/gelada-identity-library';

export interface ICreateProjectInvitationMutationGraphQLResponse {
  invitation: {
    createProjectInvitation: IDBInvitation;
  };
}

export interface ICreateProjectInvitationMutationVariables {
  invitation: IProjectInvitationVariables;
}

export interface ICreateProjectInvitationOnCompletedResponse {
  invitation: IDBInvitation;
}

export interface IHandleCreateProjectInvitationParams extends Omit<IProjectInvitationVariables, 'invitedEmail'> {
  invitedUserEmail: IDBInvitation['emailUserInvited'];
}

export interface IProjectInvitationVariables extends Pick<IDBInvitation, 'customMessage'> {
  aliasName?: IDBInvitation['communityName'];
  firstName: IDBInvitation['firstName'];
  invitedEmail: IDBInvitation['emailUserInvited'];
  lastName: IDBInvitation['lastName'];
  permission: IDBInvitation['permission'];
  projectId: IDBInvitation['project']['id'];
  title?: string;
}

export interface IUseCreateProjectInvitation extends IApolloResponseVariables {
  handleCreateProjectInvitation: (params: IHandleCreateProjectInvitationParams) => void;
}

export interface IUseCreateProjectInvitationOptions
  extends Pick<IApolloLinkOptions, 'product' | 'token'>,
    PartialPick<IApolloMutation, 'mutation'>,
    IOnCompleted<ICreateProjectInvitationOnCompletedResponse>,
    IOnError<ApolloError> {}
