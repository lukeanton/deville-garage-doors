import { gql } from '@apollo/react-hooks';

export const CREATE_PROJECT_INVITATION_MUTATION = gql`
  mutation createProjectInvitation($invitation: ProjectInvitationInputType!) {
    invitation {
      createProjectInvitation: projectInvitation(invitation: $invitation) {
        firstName
        group {
          id
          name
        }
        groupId
        id
        invitationEmailStatus
        invitedUserEmail: emailUserInvited
        invitedUserId
        lastName
        userType {
          id
          name
        }
      }
    }
  }
`;
