import { useLoggedGeladaMutation } from '@netfront/gelada-identity-library';

import { CREATE_PROJECT_INVITATION_MUTATION } from './useCreateProjectInvitation.graphql';
import {
  ICreateProjectInvitationMutationGraphQLResponse,
  ICreateProjectInvitationMutationVariables,
  IHandleCreateProjectInvitationParams,
  IUseCreateProjectInvitationOptions,
  IUseCreateProjectInvitation,
} from './useCreateProjectInvitation.interfaces';

const useCreateProjectInvitation = (options?: IUseCreateProjectInvitationOptions): IUseCreateProjectInvitation => {
  const { mutation, onCompleted, onError, product, token } = options ?? ({} as IUseCreateProjectInvitationOptions);

  const [executeCreateProjectInvitation, { loading: isLoading }] = useLoggedGeladaMutation<
    ICreateProjectInvitationMutationGraphQLResponse,
    ICreateProjectInvitationMutationVariables
  >({
    mutation: mutation ?? CREATE_PROJECT_INVITATION_MUTATION,
    options: {
      fetchPolicy: 'no-cache',
      onCompleted: (data) => {
        if (!onCompleted) {
          return;
        }

        const {
          invitation: { createProjectInvitation: invitation },
        } = data;

        onCompleted({
          invitation,
        });
      },
      onError,
    },
    product,
    token,
  });

  const handleCreateProjectInvitation = async ({
    aliasName,
    customMessage,
    invitedUserEmail,
    firstName,
    lastName,
    permission,
    projectId,
    title,
  }: IHandleCreateProjectInvitationParams) => {
    await executeCreateProjectInvitation({
      variables: {
        invitation: {
          aliasName,
          customMessage,
          invitedEmail: invitedUserEmail,
          firstName,
          lastName,
          permission,
          projectId,
          title,
        },
      },
    });
  };

  return {
    handleCreateProjectInvitation,
    isLoading,
  };
};

export { useCreateProjectInvitation };
