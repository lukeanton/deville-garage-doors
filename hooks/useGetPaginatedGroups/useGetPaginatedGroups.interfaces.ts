/* eslint-disable @typescript-eslint/indent */
import { ApolloError, ApolloQueryResult } from '@apollo/react-hooks';
import {
  IApolloConnection,
  IApolloConnectionPaginationQueryVariables,
  IApolloFetchPolicy,
  IApolloLinkOptions,
  IApolloQuery,
  IApolloResponseVariables,
  IOnCompleted,
  IOnError,
  IProjectId,
  PartialPick,
} from '@netfront/common-library';
import { DBGroupStatusType, IDBGroup } from '@netfront/gelada-identity-library';

export interface IGetPaginatedGroupsQueryIncludeVariables {
  shouldIncludeAddress?: boolean;
  shouldIncludeSmartCodes?: boolean;
  shouldIncludeUnit?: boolean;
  shouldIncludeUserType?: boolean;
}

export interface IGetPaginatedGroupsOnCompletedResponse {
  groupConnection: IApolloConnection<IDBGroup>;
}

export interface IGetPaginatedGroupsQueryGraphQLResponse {
  group: {
    getPaginatedGroups: IApolloConnection<IDBGroup>;
  };
}

export interface IGetPaginatedGroupsQueryVariables
  extends IGetPaginatedGroupsQueryIncludeVariables,
    IApolloConnectionPaginationQueryVariables,
    IProjectId {
  createdFrom?: Date;
  createdTo?: Date;
  filter?: string;
  maxUsers?: number;
  minUsers?: number;
  status?: DBGroupStatusType;
}

export interface IHandleFetchMorePaginatedGroupsParams extends IGetPaginatedGroupsQueryVariables {}

export interface IHandleGetPaginatedGroupsParams extends IGetPaginatedGroupsQueryVariables {}

export interface IUseGetPaginatedGroups extends IApolloResponseVariables {
  handleFetchMorePaginatedGroups: (
    params: IHandleFetchMorePaginatedGroupsParams,
  ) => Promise<ApolloQueryResult<IGetPaginatedGroupsQueryGraphQLResponse>>;
  handleGetPaginatedGroups: (params: IHandleGetPaginatedGroupsParams) => void;
}

export interface IUseGetPaginatedGroupsOptions
  extends IApolloFetchPolicy,
    Pick<IApolloLinkOptions, 'token'>,
    PartialPick<IApolloQuery, 'query'>,
    IOnCompleted<IGetPaginatedGroupsOnCompletedResponse>,
    IOnError<ApolloError> {}
