import { IconIdType } from 'types';

import { IAdditionalClassNames } from '../../interfaces';

export interface PreloaderProps extends IAdditionalClassNames {
  /**
   * Specify an icon id to display as the apps logo
   */
  iconId?: IconIdType;
  /**
   * The boolean whether loading is displayed
   */
  isLoading: boolean;
}
