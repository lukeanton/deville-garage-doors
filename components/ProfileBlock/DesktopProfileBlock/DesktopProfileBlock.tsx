import cx from 'classnames';

import { DesktopProfileBlockProps } from './DesktopProfileBlock.types';

import { Avatar } from '../../../components';

const DesktopProfileBlock = ({ additionalClassNames, profilePicture, bio, name }: DesktopProfileBlockProps) => {
  const profileBlockClassNames = cx('c-desktop-profile-block', additionalClassNames);

  return (
    <div className={profileBlockClassNames}>
      <div className="c-desktop-profile-block__avatar-section">
        <Avatar additionalClassNames="c-desktop-profile-block__avatar" image={{ altText: 'avatar', src: String(profilePicture) }} />
      </div>
      <div className="c-desktop-profile-block__bio-section">
        <h1 className="c-desktop-profile-block__user-name">{name}</h1>
        <p className="c-desktop-profile-block__bio">{bio}</p>
      </div>
    </div>
  );
};

export { DesktopProfileBlock };
