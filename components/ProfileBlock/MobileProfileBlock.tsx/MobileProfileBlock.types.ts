import { ProfileBlockProps, IMyProfile } from '../ProfileBlock.types';

export interface MobileProfileBlockProps extends ProfileBlockProps, IMyProfile {}
