import { ElementType } from 'react';

import { IAdditionalClassNames, ISupportiveText } from '../../interfaces';

export interface CoverLinkProps extends IAdditionalClassNames, ISupportiveText {
  /**
   * The path of the link
   */
  href: string;
  /**
   * The element used as a link
   */
  linkComponent?: ElementType;
}
