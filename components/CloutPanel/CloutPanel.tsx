import cx from 'classnames';

import { CloutPanelProps } from './CloutPanel.types';

const CloutPanel = ({ additionalClassNames, clout }: CloutPanelProps) => {
  const cloutPanelClassNames = cx('c-clout-panel', additionalClassNames);

  return (
    <ul aria-labelledby="User activity and followers" className={cloutPanelClassNames}>
      {clout.map(({ type, amount }) => (
        <li key={type} className="c-clout-panel__list-item">
          <span className="c-clout-panel__clout-amount">{amount}</span>
          <span className="c-clout-panel__clout-type">{type}</span>
        </li>
      ))}
    </ul>
  );
};

export { CloutPanel };
