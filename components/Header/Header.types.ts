import { IconIdType } from 'types';

import { IAdditionalClassNames, IAvatar, IIsMobile } from '../../interfaces';

export interface INavItems extends IAdditionalClassNames {
  /**
   * Specify a url for the link component
   */
  href: string;
  /**
   * Specify a url for the link component
   */
  iconId: IconIdType;
}

export interface HeaderProps extends IAdditionalClassNames, IAvatar, IIsMobile {
  /**
   * Specify the users avatar
   */
  appLogoSrc?: string;
  /**
   * Specify a the users handle
   */
  handle?: string;
  /**
   * Specify the type of button you would like to display
   */
  navIcons?: INavItems[];
}
