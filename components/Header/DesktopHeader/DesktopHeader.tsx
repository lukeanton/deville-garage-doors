/* eslint-disable @typescript-eslint/indent */
import cx from 'classnames';

import { DesktopHeaderProps } from './DesktopHeader.types';

import { Icon } from '../../Icon';

const DesktopHeader = ({ additionalClassNames, avatar, appLogoSrc, handle, navIcons }: DesktopHeaderProps) => {
  const desktopHeaderClassNames = cx('c-desktop-header', additionalClassNames);

  return (
    <div className={desktopHeaderClassNames}>
      <div className="c-desktop-header__wrapper">
        {appLogoSrc ? <img alt="App logo" className="c-desktop-header__image mr" src={appLogoSrc} /> : null}
        {handle ? <span className="c-desktop-header__user-name">{handle}</span> : null}
        <nav aria-label="header navigation" className="c-desktop-header__nav-icons">
          {navIcons
            ? navIcons.map(({ additionalClassNames: additionalIconClassNames, href, iconId }) => {
                return (
                  <a key={`nav-item-${String(iconId)}`} href={href}>
                    <Icon additionalClassNames={additionalIconClassNames} iconId={iconId} />
                  </a>
                );
              })
            : null}
          {avatar}
        </nav>
      </div>
    </div>
  );
};

export { DesktopHeader };
