import { Key } from 'react';

import cx from 'classnames';

import { AboutMeProps } from './AboutMe.interfaces';

const AboutMe = ({ additionalClassNames, texts = [] }: AboutMeProps) => {
  return (
    <div className={cx('c-about-me', additionalClassNames)}>
      <h2 className="c-about-me-title">About me</h2>
      {texts.map((text) => {
        return (
          <p key={text as Key} className="c-about-me__text">
            {text}
          </p>
        );
      })}
    </div>
  );
};

export { AboutMe };
