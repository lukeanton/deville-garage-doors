import { IAdditionalClassNames, IImage, ITitle } from '../../interfaces';

export interface AvatarProps extends IAdditionalClassNames, ITitle {
  /**
   * Specify an image tag and alt text to display an image
   */
  image?: IImage;
}
