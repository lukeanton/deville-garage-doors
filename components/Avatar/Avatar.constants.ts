const BRAND_COLORS = Object.freeze({
  red: 'red',
  green: 'green',
  blue: 'blue',
});

const COLOURS = Object.values(BRAND_COLORS);

export { COLOURS };
