const COMPANY_LOGOS = Object.freeze({
  'Seer Data & Analytics': 'seer-logo',
  Netfront: 'netfront-logo',
});

export { COMPANY_LOGOS };
