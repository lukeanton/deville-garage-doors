import { ReactNode, SyntheticEvent } from 'react';

export interface IAdditionalClassNames {
  /**
   * Specify additional class names that apply to the parent node
   */
  additionalClassNames?: string;
}

export interface IAvatar {
  /**
   * Specify additional class names that apply to the parent node
   */
  avatar?: ReactNode;
}

export interface ICoverLink {
  /**
   * Provide a link that will cover the element
   */
  coverLink: ReactNode;
}

export interface IIsDisabled {
  /**
   * The boolean value that decides wether the element is disabled
   */
  isDisabled?: boolean;
}

export interface IImage {
  /**
   * Specify alt text so that screen readers and search engines can identify the image
   */
  altText: string;
  /**
   * Specify the source of the image that will be displayed
   */
  src: string;
}

export interface IIsMobile {
  /**
   * The boolean value that decides if the window is a mobile size
   */
  isMobile?: boolean;
}

export interface IOnClickButton {
  onClick: (event: SyntheticEvent<HTMLButtonElement>) => void;
}

export interface IProfilePicture {
  /**
   * Provide a url that will be used as the profile picture
   */
  url: string;
}

export interface ISubTitle {
  /**
   * Specify a secondary or explanatory title that will be displayed on the page
   */
  subTitle?: string;
}

export interface ISupportiveText {
  /**
   * Specify supportive text for screen readers
   */
  supportiveText: string;
}

export interface IText {
  /**
   * Specify a text to be displayed on the screen
   */
  IText?: string;
}

export interface ITitle {
  /**
   * Specify a title that will be displayed on the page
   */
  title?: string;
}
